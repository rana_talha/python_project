from django.shortcuts import render
from .models import Company, Employee
from rest_framework import viewsets
from .serializer import CompanySerializer, EmployeeSerializer


# Create your views here.
class CompanyViewSet(viewsets.ModelViewSet):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()


class EmployeeViewSet(viewsets.ModelViewSet):
    serializer_class = EmployeeSerializer
    queryset = Employee.objects.all()