from . views import CompanyViewSet, EmployeeViewSet
from django.urls import path, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('company', CompanyViewSet, basename='company')

router1 = DefaultRouter()
router1.register('employee', EmployeeViewSet, basename='employee')

urlpatterns = [
    path('company/', include(router.urls)),
    path('company/<int:pk>/', include(router.urls)),
    path('employee/', include(router1.urls)),
    path('employee/<int:pk>/', include(router1.urls))
]