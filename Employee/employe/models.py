from django.db import models


# Create your models here.
class Company(models.Model):
    company_name = models.CharField(max_length=100)
    pub_date = models.DateField(auto_created=True)

    def __str__(self):
        return self.company_name


class Employee(models.Model):
    first_name = models.CharField(max_length=100, help_text='Enter Your First Name')
    last_name = models.CharField(max_length=100, help_text='Enter Your Last Name')
    GENDER_MALE = 0
    GENDER_FEMALE = 1
    GENDER_CHOICES = [(GENDER_MALE, 'Male'), (GENDER_FEMALE, 'Female')]
    gender = models.IntegerField(choices=GENDER_CHOICES)
    dob = models.DateField(help_text='Enter your date of birth')
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    designation = models.CharField(max_length=100, )
    contact = models.IntegerField(help_text='Enter Phone Number')
    address = models.CharField(max_length=100, help_text='Enter your Street Address')
    city = models.CharField(max_length=100)
    country = models.CharField(max_length=100)

    def __str__(self):
        return self.first_name
